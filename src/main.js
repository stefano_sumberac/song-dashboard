import './assets/base.css'
import './assets/main.css'
import 'nprogress/nprogress.css'

import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'
import VeeValidatePlugin from './includes/validation'
import { auth } from "./includes/firebase";
import i18n from "./includes/i18n"
import { registerSW } from "virtual:pwa-register"
import GlobalComponents from './includes/_globals'
import progressBar from './includes/progress-bar'

/* import the fontawesome core */
import { library } from '@fortawesome/fontawesome-svg-core'

/* import font awesome icon component */
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

/* import specific icons */
import { faUserSecret } from '@fortawesome/free-solid-svg-icons'

/* import specific icons */
import { fas } from '@fortawesome/free-solid-svg-icons'

/* add icons to the library */
library.add(faUserSecret)
library.add(fas)

registerSW({ immediate: true })

progressBar(router)

let app;

auth.onAuthStateChanged(() => {
  if(!app){
    app = createApp(App)
    .component('font-awesome-icon', FontAwesomeIcon)
  
    app.use(createPinia())
    app.use(router)
    app.use(VeeValidatePlugin)
    app.use(i18n)
    app.use(GlobalComponents)
    
    app.mount('#app')  
  }
});

